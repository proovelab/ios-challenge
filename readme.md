#Develop simple VK client

##Minimal requirements:

###Screens:

* *User authentication (Oauth 2.0)*: the controller contains **Login with VK** button to go VK access control page.
* *Posts list*: the controller shows posts pf user's news tape with user's names, dates, avatars, content (text or/and images) and numbers of likes and re-posts. The screen appears after authentication.
* *Post details*: the controller shows full data of the post with user's names, dates, avatars, content (text or/and images) and numbers of likes and re-posts.

###Features:

* MVC architecture
* Using [DRY](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself) / [KISS](https://en.wikipedia.org/wiki/KISS_principle) / [YAGNI](https://en.wikipedia.org/wiki/You_aren%27t_gonna_need_it) principles
* Using [Cocoa Pods](https://cocoapods.org) to work with third-party components
* Have to use [VK SDK](https://new.vk.com/dev/ios_sdk)
* UI have to realised with Storyboard/Autolayout
* Pull to load more - pull down to load more posts
* Pull to refresh - pull up to refresh
* Have to be realized login and logout procedures
* Using CoreData for local storage
* Images must be cached
* Show GСD using for images loading

###VCS:

* You need to create git repo on any public service and develop project with it.
* Frequency of comments will be rated.
* Readme.md must contain a deployment and running instruction 

###Time requirements:
* You can spend less that 8 hours.

##Additional requirements

###Features:

* iPhone/iPad support
* Portrait/Landscape support
* Using [VK API](https://new.vk.com/dev/apiusage) + [AFNetworking](https://github.com/AFNetworking/AFNetworking) instead [VK SDK](https://new.vk.com/dev/ios_sdk)
* Using [SQLite](https://www.sqlite.org) instead CoreData
* Few classes have to be covered by unit-tests